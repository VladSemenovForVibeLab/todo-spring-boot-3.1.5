package com.sfv.todo.todo;

import com.sfv.todo.todo.dto.CreateTodoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {
    @Autowired
    private TodoService todoService;

    @PostMapping
    public ResponseEntity<Todo> create(@RequestBody CreateTodoDto dto) {
        return new ResponseEntity<>(todoService.create(dto), HttpStatus.CREATED);
    }

    @GetMapping("{id}")
    public ResponseEntity<Todo> findOne(@PathVariable Integer id) {
        var todo = todoService.findOne(id)
                .orElseThrow();
        return ResponseEntity.ok(todo);
    }
    @GetMapping
    public ResponseEntity<List<Todo>> findAll() {
        List<Todo> all = todoService.findAll();
        return ResponseEntity.ok(all);
    }

    @PutMapping("{id}")
    public ResponseEntity<Todo> update(
            @PathVariable("id") Integer id,
            @RequestBody CreateTodoDto dto) {
        return new ResponseEntity<>(todoService.update(id, dto), HttpStatus.OK);
    }
    @DeleteMapping("{id}")
    public ResponseEntity<Boolean> delete(@PathVariable("id") Integer todoId){
        todoService.delete(todoId);
        return ResponseEntity.ok(true);
    }
}
